
https://gitlab.upt.ro/laurentiu.mot/SistemPozitionareApp.git

Aplicația mobilă Android:
- Se descarcă repository-ul local.
- Se deschide IDE Android Studio și se deschide proiectul din meniul File->Open->calea catre proiect
- Se configurează "Device-ul de tip Smartphone", apoi se rulează de la butonul RUN, care generează un fișier „.apk”

Aplicația Arduino:
- Se deschide fișierul de tip Ardiono, cu extensia „.ino”;
- Se conectează placa Arduino pentru încărcarea codului pe placă;
- După compilare și link-editare se generează automat pe placa Arduino, fișierul binar de tip „.hex”
