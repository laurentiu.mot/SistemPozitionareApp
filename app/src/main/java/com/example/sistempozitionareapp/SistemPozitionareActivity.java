package com.example.sistempozitionareapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class SistemPozitionareActivity extends AppCompatActivity {
    private static final String TAG = "SistemPozitionareActivity";
    private static final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    // Declaratiile de membri

    private ProgressDialog progress;
    String address = null;
    private BluetoothAdapter bluetoothAdapter;

    private BluetoothDevice bluetoothDevice;
    private BluetoothSocket bluetoothSocket = null;
    private boolean isBtConnected = false;
    private OutputStream outputStream;
    private InputStream inputStream;
    private TextView voltageTextView, currentTextView, powerValueTextView, horizontalValueTextView, verticalValueTextView;
    private SeekBar horizontalSeekBar, verticalSeekBar;
    private EditText horizontalEditText, verticalEditText;
    private Button horizontalSendButton, verticalSendButton, automaticTuningButton, manualRegulatorButton;
    private Handler handler;
    private byte[] mmBuffer; // mmBuffer magazin pentru flux


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sistem_pozitionare_view);


        voltageTextView = findViewById(R.id.voltageTextView);
        currentTextView = findViewById(R.id.currentTextView);
        horizontalValueTextView = findViewById(R.id.horizontalValueTextView);
        verticalValueTextView = findViewById(R.id.verticalValueTextView);
        powerValueTextView = findViewById(R.id.powerTextView);
        horizontalSeekBar = findViewById(R.id.horizontalSeekBar);
        verticalSeekBar = findViewById(R.id.verticalSeekBar);
        horizontalEditText = findViewById(R.id.horizontalEditText);
        verticalEditText = findViewById(R.id.verticalEditText);
        horizontalSendButton = findViewById(R.id.horizontalSendButton);
        verticalSendButton = findViewById(R.id.verticalSendButton);
        automaticTuningButton = findViewById(R.id.automaticRegulatorButton);
        manualRegulatorButton = findViewById(R.id.manualRegulatorButton);
        horizontalValueTextView.setTextColor(getResources().getColor(R.color.grey));
        verticalValueTextView.setEnabled(false);
        horizontalSeekBar.setEnabled(false);
        verticalSeekBar.setEnabled(false);
        horizontalEditText.setEnabled(false);
        verticalEditText.setEnabled(false);
        horizontalSendButton.setEnabled(false);
        verticalSendButton.setEnabled(false);

        horizontalSeekBar.setProgress(90);
        horizontalValueTextView.setText("90");
        horizontalEditText.setText("90");
        horizontalEditText.setSelection(2);
        verticalSeekBar.setProgress(90);
        verticalValueTextView.setText("90");
        verticalEditText.setText("90");
        verticalEditText.setSelection(2);

//        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Intent intent = getIntent();
        address = intent.getStringExtra(BluetoothConnectActivity.EXTRA_ADDRESS);

        new ConnectBT().execute();
//        checkBluetoothState();

        horizontalSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Trimite comanda pentru controlul motorului orizontal catre Arduino
//                sendCommand("c" + "H" + progress);
                horizontalValueTextView.setText(String.valueOf(progress));
                horizontalEditText.setText(String.valueOf(progress));
                horizontalEditText.setSelection(String.valueOf(progress).length());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        verticalSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Trimite comanda pentru controlul motorului vertical catre Arduino
//                sendCommand("c" + "V" + progress);
                verticalValueTextView.setText(String.valueOf(progress));
                verticalEditText.setText(String.valueOf(progress));
                verticalEditText.setSelection(String.valueOf(progress).length());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        horizontalSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = horizontalEditText.getText().toString();
                if (!value.isEmpty()) {
                    int progress = Integer.parseInt(value);
                    if (progress >= 0 && progress <= 180) {
                        horizontalSeekBar.setProgress(progress);
                        horizontalValueTextView.setText(value);
                        horizontalEditText.setSelection(value.length());
//                        sendCommand("c" + "H" + progress);
                    } else {
                        Toast.makeText(SistemPozitionareActivity.this, "Valoare invalidă. Vă rugăm sa introduceți o valoare între 0 și 180", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        verticalSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = verticalEditText.getText().toString();
                if (!value.isEmpty()) {
                    int progress = Integer.parseInt(value);
                    if (progress >= 0 && progress <= 180) {
                        verticalSeekBar.setProgress(progress);
                        verticalValueTextView.setText(value);
                        verticalEditText.setSelection(value.length());
//                        sendCommand("c" + "V" + progress);
                    } else {
                        Toast.makeText(SistemPozitionareActivity.this, "Valoare invalidă. Vă rugăm sa introduceți o valoare între 0 și 180", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        horizontalValueTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String value = s.toString();
                int progress = Integer.parseInt(value);
                sendCommand("c" + "H" + progress);
            }
        });

        verticalValueTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String value = s.toString();
                int progress = Integer.parseInt(value);
                sendCommand("c" + "V" + progress);
            }
        });

        automaticTuningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verticalValueTextView.setEnabled(false);
                horizontalSeekBar.setEnabled(false);
                verticalSeekBar.setEnabled(false);
                horizontalEditText.setEnabled(false);
                verticalEditText.setEnabled(false);
                horizontalSendButton.setEnabled(false);
                verticalSendButton.setEnabled(false);
                sendCommand("A");
            }
        });

        manualRegulatorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verticalValueTextView.setEnabled(true);
                horizontalSeekBar.setEnabled(true);
                horizontalSeekBar.setEnabled(true);
                verticalSeekBar.setEnabled(true);
                horizontalEditText.setEnabled(true);
                verticalEditText.setEnabled(true);
                horizontalSendButton.setEnabled(true);
                verticalSendButton.setEnabled(true);
                sendCommand("M");
            }
        });
    }

    private class ReadValuesTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            byte[] buffer = new byte[1024];
            int bytes;
            StringBuilder receivedDataBuilder = new StringBuilder();

            while (true) {
                try {
                    // Citeste datele din fluxul de intrare
                    bytes = inputStream.read(buffer);

                    // Combina fragmentele de date intr-un sir complet
                    String receivedDataFragment = new String(buffer, 0, bytes);
                    receivedDataBuilder.append(receivedDataFragment);

                    // Verifica daca sirul complet contine date valide
                    String receivedData = receivedDataBuilder.toString();
                    if (receivedData.contains("T") && receivedData.contains("C")) {
                        // Actualizeaza interfata utilizator sau efectueaza alte actiuni cu datele primite
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // Verifica datele primite si actualizeaza TextView-urile corespunzatoare
                                int voltageStartIndex = receivedData.indexOf("T") + 1;
                                int voltageEndIndex = receivedData.indexOf("C");
                                int currentStartIndex = receivedData.indexOf("C") + 1;

                                if (voltageStartIndex < voltageEndIndex && currentStartIndex < receivedData.length()) {
                                    String voltage = receivedData.substring(voltageStartIndex, voltageEndIndex);
                                    String current = receivedData.substring(currentStartIndex);
                                    String putere;

                                    try {
                                        // Conversia valorilor de tensiune si curent in float
                                        float voltageValue = Float.parseFloat(voltage);
                                        float currentValue = Float.parseFloat(current);

                                        // Calcularea valorii puterii
                                        float powerValue = voltageValue * currentValue;
                                        putere = Float.toString(powerValue) + "W";

                                        voltageTextView.setText("Tensiunea: " + voltage + "V");
                                        currentTextView.setText("Curentul: " + current + "mA");
                                        powerValueTextView.setText("Puterea: " + putere);
                                    } catch (NumberFormatException e) {
                                        // Valorile de tensiune si/sau curent nu sunt numerice valide
                                        voltageTextView.setText("Tensiunea: Valoare nevalidă");
                                        currentTextView.setText("Curentul: Valoare nevalidă");
                                        powerValueTextView.setText("Puterea: Valoare nevalidă");
                                    }
                                } else {
                                    voltageTextView.setText("Tensiunea: Valoare nevalidă");
                                    currentTextView.setText("Curentul: Valoare nevalidă");
                                    powerValueTextView.setText("Puterea: Valoare nevalidă");
                                }

                                // Resetare sir de date
                                receivedDataBuilder.setLength(0);
                                handler = new Handler();
                                handler.postDelayed(this, 500); // re-schedule the handler to run after 500 ms
                            }
                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Eroare la citirea datelor de la Arduino: " + e.getMessage());
                    break;
                }
            }
            return null;
        }
    }



    private void msg (String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    private void sendCommand(String command) {
        try {
            System.out.println("Valoare: " + command);
            System.out.println("Comanda de Bytes: " + command.getBytes());
            outputStream.write(command.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Metoda pentru conectarea la dispozitivul Bluetooth
    private class ConnectBT extends AsyncTask<Void, Void, Void> {
        private boolean ConnectSuccess = true;
        ReadValuesTask readvaluestask = new ReadValuesTask();

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(SistemPozitionareActivity.this, "Conectare...", "Asteapta puțin!");
        }


        @Override
        protected Void doInBackground(Void... devices) {
            try {
                if (bluetoothSocket == null || !isBtConnected) {
                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice dispositivo = bluetoothAdapter.getRemoteDevice(address);
                    bluetoothSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(PORT_UUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    bluetoothSocket.connect();

                    // Get the input and output streams
                    inputStream = bluetoothSocket.getInputStream();
                    outputStream = bluetoothSocket.getOutputStream();
                }
            } catch (IOException e) {
                ConnectSuccess = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                msg("Conexiune eșuată. Încearcă din nou.");
                finish();
            } else {
                msg("Conectat");
                readvaluestask.execute();
                isBtConnected = true;
            }
            progress.dismiss();
        }
    }

}
